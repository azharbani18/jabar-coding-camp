//Soal 1
console.log("Soal 1");

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

for (var i = 1; i < daftarHewan.length; i++)
    for (var j = 0; j < i; j++)
        if (daftarHewan[i] < daftarHewan[j]) {
          var x = daftarHewan[i];
          daftarHewan[i] = daftarHewan[j];
          daftarHewan[j] = x;
        }

for (var i = 0; i < daftarHewan.length; i++) {
    console.log(daftarHewan[i]);
}

console.log("\n");

//Soal 2
console.log("Soal 2");

function introduce(data_perkenalan) {
    return "Nama saya " + data_perkenalan.name +", umur saya " + data_perkenalan.age + " tahun, alamat saya di "
            + data_perkenalan.address + ", dan saya punya hobby yaitu " +  data_perkenalan.hobby;
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data);
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

console.log("\n");

//Soal 3
console.log("Soal 3");

// defining vowels
const vowels = ["a", "e", "i", "o", "u"];

function hitung_huruf_vokal(string) {
    let count = 0;
    let str = string.toLowerCase();

    for (var i = 0; i < str.length; i++) {
        if (vowels.includes(str[i])) {
            count++;
        }
    }
    return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

console.log("\n");

//Soal 4
console.log("Soal 4");

function hitung(angka) {
    return (2 * angka) - 2;
}

console.log( hitung(0) ) //-2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8