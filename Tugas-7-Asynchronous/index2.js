var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise
readBooksPromise(10000, books[0])
.then(function(val1){
	return readBooksPromise(val1, books[1]);
})
.then(function(val2){
	return readBooksPromise(val2, books[2]);
})
.then(function(val3){
	return readBooksPromise(val3, books[3]);
});