//Soal 1

var tanggal = 29;
var bulan = 2;
var tahun = 2020;
function next_date(tgl, bln, thn) {
    var namaBulan = [" ","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
    
    if(checkLeapYear(thn)) {
        if(bln == 2){
            if(tgl == 29){
                tgl = 1;
                bln++;
            } else{
                tgl++;
            }
        } else if(bln == 12){
            if(tgl == 31){
                tgl = 1;
                bln = 1;
                thn = thn + 1;
            } else {
                tgl++;
            }
        }
         else if(bln = 1 || bln == 3 || bln == 5  || bln == 7  || bln == 8  || bln == 10) {
            if(tgl == 31){
                tgl = 1;
                bulan++;
            } else {
                tgl++;
            }
        } else if(bln == 4 || bln == 6  || bln == 9  || bln == 11){
            if(tgl == 30){
                tgl = 1;
                bulan++;
            } else {
                tgl++;
            }
        }
    } else {
        if(bln == 2){
            if(tgl == 29){
                tgl = 1;
                bln++;
            } else{
                tgl++;
            }
        } else if(bln == 12){
            if(tgl == 31){
                tgl = 1;
                bln = 1;
                thn = thn + 1;
            } else {
                tgl++;
            }
        }
         else if(bln = 1 || bln == 3 || bln == 5  || bln == 7  || bln == 8  || bln == 10) {
            if(tgl == 31){
                tgl = 1;
                bulan++;
            } else {
                tgl++;
            }
        } else if(bln == 4 || bln == 6  || bln == 9  || bln == 11){
            if(tgl == 30){
                tgl = 1;
                bulan++;
            } else {
                tgl++;
            }
        }

    }
    return tgl + " " + namaBulan[bln] + " "+ thn;
}

console.log(next_date(tanggal, bulan, tahun));

function checkLeapYear(year) {
    //three conditions to find out the leap year
    if ((0 == year % 4) && (0 != year % 100) || (0 == year % 400)) {
        return true;
    } else {
        return false;
    }
}

//Soal 2

var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok";
var kalimat_2 = "Saya Iqbal";

function jumlah_kata(str) {
    var i = 0;
    var numberOfWords = 1;

    while (i < str.length) {
        if(str.substring(i, i+1) == " ") {
            numberOfWords++;
            i++;
        }
        if(str.substring(i,i+1) == "\n") {
            numberOfWords++;
            i++;
        }
        i++;
    }
    return numberOfWords;
}

console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));
