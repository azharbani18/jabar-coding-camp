//Soal 1
console.log('Soal 1');

const luasPersegiPanjang = (panjang, lebar) => {
    return panjang * lebar;
};

const kelilingPersegiPanjang = (panjang, lebar) => {
    const sisi = 2;
    return sisi*(panjang + lebar);
};

let _panjang = 10;
let _lebar   = 7;

console.log(luasPersegiPanjang(_panjang,_lebar));
console.log(kelilingPersegiPanjang(_panjang,_lebar));

console.log("\n");


//Soal 2
console.log("Soal 2");

const newFunction = (firstName, lastName) => {
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(`${firstName} ${lastName}`);
      }
    }
  }
   
//Driver Code 
newFunction("William", "Imoh").fullName();

console.log("\n");


//Soal 3
console.log("Soal 3");

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject;

// Driver code
console.log(firstName, lastName, address, hobby);

console.log("\n");

//Soal 4
console.log("Soal 4");

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined);

console.log("\n");

//Soal 5
console.log("Soal 5");

const planet = "earth"; 
const view = "glass";
var before = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet}`;

console.log(before);
