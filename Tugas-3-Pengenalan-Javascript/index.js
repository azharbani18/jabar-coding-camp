//Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var array_pertama = pertama.split(" ");
var array_kedua   = kedua.split(" ");

var kalimat1 = array_pertama[0].concat(" " + array_pertama[2]);
var kalimat2 = array_kedua[0].concat(" " + array_kedua[1].toUpperCase());
console.log(kalimat1.concat(" " + kalimat2));

//Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
console.log(parseInt((kataPertama - kataKedua - kataKetiga) * kataKeempat)); //Output 24 Integer

//Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua   = kalimat.substring(4,14);
var kataKetiga  = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24);
var kataKelima  = kalimat.substring(25,31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
