//Soal 1
console.log('Soal 1');
var nilai = 69;

if(nilai >= 85) {
    console.log("indeks A");
} else if (nilai >= 75 && nilai < 85) {
    console.log("indeks B");
} else if (nilai >= 65 && nilai < 75) {
    console.log("indeks C");
} else if (nilai >= 55 && nilai < 65) {
    console.log("indeks D");
} else if (nilai < 55) {
    console.log("indeks E");
}

console.log('\n');

//Soal 2
console.log('Soal 2');
var tanggal = 18;
var bulan = 8;
var tahun = 2001;

switch(bulan) {
    case 1 : {
        console.log(tanggal + " Januari " + tahun);
        break;
    }
    case 2 : {
        console.log(tanggal + " Februari " + tahun);
        break;
    }
    case 3 : {
        console.log(tanggal + " Maret " + tahun);
        break;
    }
    case 4 : {
        console.log(tanggal + " April " + tahun);
        break;
    }
    case 5 : {
        console.log(tanggal + " Mei " + tahun);
        break;
    }
    case 6 : {
        console.log(tanggal + " Juni " + tahun);
        break;
    }
    case 7 : {
        console.log(tanggal + " Juli " + tahun);
        break;
    }
    case 8 : {
        console.log(tanggal + " Agustus " + tahun);
        break;
    }
    case 9 : {
        console.log(tanggal + " September " + tahun);
        break;
    }
    case 10 : {
        console.log(tanggal + " Oktober " + tahun);
        break;
    }
    case 11 : {
        console.log(tanggal + " November " + tahun);
        break;
    }
    case 12 : {
        console.log(tanggal + " Desember " + tahun);
        break;
    }
    default : { console.log("bulan tidak terdefinisi"); break;}
}

console.log('\n');

//Soal 3
    //n = 3
    console.log('Soal 3\nUntuk n = 3 :');
    let hasil = '';
    var n = 3;
    for (let i = 0; i < n; i++) {
        for (let j = 0; j <= i; j++) {
            hasil += '#';
        }
        hasil += '\n';
    }

    console.log(hasil);

    console.log('\n');

    //n = 7
    console.log('Untuk n = 7 :');
    hasil = '';
    var n = 7;
    for (let i = 0; i < n; i++) {
        for (let j = 0; j <= i; j++) {
            hasil += '#';
        }
        hasil += '\n';
    }

    console.log(hasil);

//Soal 4
console.log('Soal 4');    

var string1 = ' - I love programming';
var string2 = ' - I love Javascript';
var string3 = ' - I love VueJS';
var batas   = '';

//untuk m = 3
console.log('Untuk m = 3 :');
var m = 3;

for(let i = 1; i <= m; i++) {
    if(i%3 == 1) {
        console.log(i + string1);
    } else if(i%3 == 2){
        console.log(i + string2);
    } else if (i%3 == 0){
        console.log(i + string3);
        batas += '===';
        console.log(batas);
    }
}

console.log('\n');
//untuk m = 5
console.log('Untuk m = 5 :');
batas = '';
m = 5;

for(let i = 1; i <= m; i++) {
    if(i%3 == 1) {
        console.log(i + string1);
    } else if(i%3 == 2){
        console.log(i + string2);
    } else if (i%3 == 0){
        console.log(i + string3);
        batas += '===';
        console.log(batas);
    }
}

console.log('\n');

//untuk m = 7
console.log('Untuk m = 7 :');
batas = '';
m = 7;

for(let i = 1; i <= m; i++) {
    if(i%3 == 1) {
        console.log(i + string1);
    } else if(i%3 == 2){
        console.log(i + string2);
    } else if (i%3 == 0){
        console.log(i + string3);
        batas += '===';
        console.log(batas);
    }
}

console.log('\n');

//untuk m = 10
console.log('Untuk m = 10 :');
batas = '';
m = 10;

for(let i = 1; i <= m; i++) {
    if(i%3 == 1) {
        console.log(i + string1);
    } else if(i%3 == 2){
        console.log(i + string2);
    } else if (i%3 == 0){
        console.log(i + string3);
        batas += '===';
        console.log(batas);
    }
}